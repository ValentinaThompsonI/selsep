package week3.day1;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IRCTCFourthLinkClick {
	public static void main(String[] args) throws InterruptedException {
	//Access the web drivers
	System.setProperty("webdriver.chrome.driver","C:\\GradleProject\\Workspace\\Selenium\\Drivers\\chromedriver.exe");
	//Create new ChromeDriver Object
	ChromeDriver driver=new ChromeDriver();
	//Open URL
	driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");	
	List<WebElement> tag = driver.findElementsByTagName("a");
	int size = tag.size();
	System.out.println(size);
	tag.get(9).click();

}
}

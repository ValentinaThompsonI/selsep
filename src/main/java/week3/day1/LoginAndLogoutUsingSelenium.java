package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LoginAndLogoutUsingSelenium {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","C:\\GradleProject\\Workspace\\Selenium\\Drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		//input username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//input password
		driver.findElementById("password").sendKeys("crmsfa");
		//submit click
		driver.findElementByClassName("decorativeSubmit").click();
		//select link
		driver.findElementByLinkText("CRM/SFA").click();
		//select leads
		driver.findElementByLinkText("Leads").click();
		//select CreateLeads;
		driver.findElementByLinkText("Create Lead").click();
		//company name
		driver.findElementById("createLeadForm_companyName").sendKeys("Cognizant");
		//first name
		driver.findElementById("createLeadForm_firstName").sendKeys("Valentina");
		//last name
		driver.findElementById("createLeadForm_lastName").sendKeys("Thompson");
		//Source dropdown  select by visable text
		WebElement source=driver.findElementById("createLeadForm_dataSourceId");
		Select dd=new Select(source);
		dd.selectByVisibleText("Public Relations");
		//MarketingCampaign
		WebElement source1=driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd1=new Select(source1);
		dd1.selectByValue("CATRQ_AUTOMOBILE");	
		//local first name firstNameLocal
		driver.findElementById("firstNameLocal").sendKeys("Valentina");
		//local last name lastNameLocal
		driver.findElementById("lastNameLocal").sendKeys("Thompson");
		//Salutation createLeadForm_personalTitle
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Mrs.");
		//Tittle: createLeadForm_generalProfTitle
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Mrs.");
		//department createLeadForm_departmentName
		driver.findElementById("createLeadForm_departmentName").sendKeys("NFT COE");
		//Annual income createLeadForm_annualRevenue
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("41500");
		//Preferred currency
		source=driver.findElementById("createLeadForm_currencyUomId");
		Select dd4=new Select(source);
		dd4.selectByValue("INR");
		//industry
		WebElement source2=driver.findElementById("createLeadForm_industryEnumId");
		Select dd5=new Select(source2);
		dd5.selectByIndex(9);
		//number of employees createLeadForm_numberEmployees
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("20");
		//
		source=driver.findElementById("createLeadForm_ownershipEnumId");
		Select dd6=new Select(source);
		dd6.deselectByValue("OWN_PUBLIC_CORP");
		
		/*String st="";
		List<WebElement> alloption=dd2.getOptions();
		for (WebElement webElement : alloption) {
			st=webElement.getText();
			if(st.startsWith("M"))
				System.out.println(st);
		}*/
		
		//driver.find
		
		
		
		
		
		//driver.findElementByClassName("smallSubmit").click();
		
		
		
	}

}
package week3.day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IRCTCUserRegistrationForm {

	public static void main(String[] args) throws Throwable {
		//Access the web drivers
		System.setProperty("webdriver.chrome.driver","C:\\GradleProject\\Workspace\\Selenium\\Drivers\\chromedriver.exe");
		//Create new ChromeDriver Object
		ChromeDriver driver=new ChromeDriver();
		//Open URL
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		//Username
		driver.findElementById("userRegistrationForm:userName").sendKeys("Valentina");
		//Password
		driver.findElementById("userRegistrationForm:password").sendKeys("Avanthiks@12");
		//Confirm Password
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Avanthiks@12");
		//Security Question Dropdown
		WebElement source=driver.findElementById("userRegistrationForm:securityQ");
		Select dd=new Select(source);
		dd.selectByVisibleText("Where did you first meet your spouse?");
		//Security Question Answer
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Orkut");
		//Language Preferance
		WebElement source1=driver.findElementById("userRegistrationForm:prelan");
		Select dd1=new Select(source1);
		dd1.selectByVisibleText("English");
		//First name
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Valentina");
		//Last Name
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Thompson I");
		//Gender 
		driver.findElementById("userRegistrationForm:gender:1").click();
		//Marital Status
		driver.findElementById("userRegistrationForm:maritalStatus:0").click();
		//DOB->Day
		WebElement source2=driver.findElementById("userRegistrationForm:dobDay");
		Select dd2=new Select(source2);
		dd2.selectByVisibleText("01");
		//DOB->Month
		WebElement source3=driver.findElementById("userRegistrationForm:dobMonth");
		Select dd3=new Select(source3);
		dd3.selectByVisibleText("APR");
		//DOB->Year
		WebElement source4=driver.findElementById("userRegistrationForm:dateOfBirth");
		Select dd4=new Select(source4);
		dd4.selectByVisibleText("1989");
		//Occupation 
		WebElement sc=driver.findElementById("userRegistrationForm:occupation");
		Select d=new Select(sc);
		d.selectByVisibleText("Private");
		//Adaar No 
		driver.findElementById("userRegistrationForm:uidno").sendKeys("");
		//Pan Card
		driver.findElementById("userRegistrationForm:idno").sendKeys("ALVPV3173F");
		//Dropdown Countries
		WebElement source5=driver.findElementById("userRegistrationForm:countries");
		Select dd5=new Select(source5);
		dd5.selectByVisibleText("India");
		//Email ID 
		driver.findElementById("userRegistrationForm:email").sendKeys("valentinathmpsn@gmail.com");
		//Mobile No
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9884634329");
		//Nationality 
		WebElement source6=driver.findElementById("userRegistrationForm:nationalityId");
		Select dd6=new Select(source6);
		dd6.selectByVisibleText("India");
		//Address 
		driver.findElementById("userRegistrationForm:address").sendKeys("Plot No:8");
		//Street Name 
		driver.findElementById("userRegistrationForm:street").sendKeys("Ram St, Shanmuga Nagar");
		//Area
		driver.findElementById("userRegistrationForm:area").sendKeys("P C Colony, Pozhichallur");
		//Pincode
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600074",Keys.TAB);
		Thread.sleep(5000);
		//City
		WebElement source7=driver.findElementById("userRegistrationForm:cityName");
		Select dd7=new Select(source7);
		dd7.selectByIndex(1);
		Thread.sleep(5000);
		//Postoffice
		WebElement source8=driver.findElementById("userRegistrationForm:postofficeName");
		Select dd8=new Select(source8);
		dd8.selectByIndex(1);
		//Landline
		driver.findElementById("userRegistrationForm:landline").sendKeys("2266448855");
		//Select residence to office userRegistrationForm:resAndOff:0
		driver.findElementById("userRegistrationForm:resAndOff:0").click();

	}

}

package week2.day1;

import java.util.ArrayList;
import java.util.List;

public class ListPrintReverse {

	public static void main(String[] args) {
		// Program to print the values stored in List in reverse order
		int length=0;
		List <Integer> numbers=new ArrayList<>();
		numbers.add(5);
		numbers.add(10);
		numbers.add(2);
		numbers.add(22);
		numbers.add(50);
		length=numbers.size();
	    for (int i=length-1;i>=0;i--) {
	    	System.out.println(numbers.get(i));
	    				
		}
		
	

	}

}

package week2.day1;

import java.util.Scanner;

public class StringPalindrome {

	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the string to check if palindrome or not");
         String st= sc.nextLine();
         String reverse="";
         int length=st.length();
         for(int i=length-1;i>=0;i--)
         {
        	 reverse=reverse+st.charAt(i);
         }
         if(st.equals(reverse))
        	 System.out.println("The string entered is a palindrome");
         else
        	 System.out.println("The string entered is not a palindrome");
         
         sc.close();
          
	}

}

package week2.day1;

import java.util.Scanner;

public class ArithmeticOperations {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int sum=0;
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the first value to perform the arithmetic operation");
         int st= sc.nextInt();
         System.out.println("Enter the second value to perform the arithmetic operation");
         int st1= sc.nextInt();
         System.out.println("Enter the Arithmetic operation to be performed (Add/Subtract/Multiply/Divide)");
         String sti= sc.next();
         sc.close();
         try{
         switch (sti) {
         case "Add": 
        	 sum=st+st1;
        	 System.out.println(st+" + "+st1+"="+sum);
        	 break;
         case "Subtract":
        	 sum=st-st1;
        	 System.out.println(st1+" - "+st1+"="+sum);
        	 break;
         case "Multiply":
        	 sum=st*st1;
        	 System.out.println(st1+" * "+st1+"="+sum);
        	 break;
        case "Divide":
             sum=st/st1;
             System.out.println(st1+" / "+st1+"="+sum);
             break;
             
          default:
        	  System.out.println("Wrong input");
         }
         }catch(ArithmeticException e)
         {
        	System.out.println("Please do not divide by zero");
         }
         catch(Exception e)
         {
        	 System.out.println("Something went wrong, try again later");
         }
         }
	}



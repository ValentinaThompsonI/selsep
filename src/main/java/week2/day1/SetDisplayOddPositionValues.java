package week2.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class SetDisplayOddPositionValues {

	public static void main(String[] args) {
		// Program to create a Set object and display the records in odd positions
		Set<Integer> numbers=new TreeSet<>();
		numbers.add(15);
		numbers.add(10);
		numbers.add(2);
		numbers.add(26);
		numbers.add(52);
		//numbers.
		List <Integer> lsnum=new ArrayList<>();
		lsnum.addAll(numbers);
		
		for (int i=0;i<lsnum.size();i++) {
			if(i%2 !=0)
				System.out.println(lsnum.get(i));
		}
		

	}

}

package week2.day1;

import java.util.ArrayList;
import java.util.List;

public class  ListImplementation{

	public static void main(String[] args) {
		
		//Program to find the count of the duplicate value bahubali
		int count=0;
		List <String> movies=new ArrayList<>();
		movies.add("bahubali");
		movies.add("u turn");
		movies.add("raja ranguskhan");
		movies.add("bahubali");
		movies.add("kolamavu kokila");
		movies.add("bahubali");
		for (String eachmovies : movies) {
			if(eachmovies.contains("bahubali"))
				count++;
		}
		
		System.out.println("Bahubali is available "+count+ " times");
	}

}

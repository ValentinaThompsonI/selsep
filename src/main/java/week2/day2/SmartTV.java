package week2.day2;

public class SmartTV extends Televison {
	public void connectWIFI()
	{
		System.out.println("Connecting WIFI...");
	}
	public void installApp(String appName)
	{
		System.out.println("Installing "+ appName);
	}
	public void connectToInternet()
	{
		System.out.println("Connecting to Internet");
	
	}
	@Override
	public void watchTV()
	{
	System.out.println("Watching Smart TV");
	}
	
	public static void main(String[] args) {
		SmartTV stv=new SmartTV();
		Televison tv=new Televison();
		//SmartTV object
		System.out.println("Smart TV");
		stv.connectToInternet();
		stv.connectWIFI();
		stv.installApp("YouTube");
		stv.changeChannel();
		stv.increaseVolume();
		stv.isColourTV();
		stv.switchOn();
		stv.watchSongs();
		stv.watchTV();
		
		//Televison object
		System.out.println("Television");
		tv.changeChannel();
		tv.increaseVolume();
		tv.isColourTV();
		tv.switchOn();
		tv.watchSongs();
		tv.watchTV();
		//interfase
		System.out.println(" Entertainment");
		Entertainment ent=new Televison();
		ent.increaseVolume();
		ent.isColourTV();
		ent.watchSongs();
	}
}

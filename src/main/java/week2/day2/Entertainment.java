package week2.day2;

public interface Entertainment {
	public void isColourTV();
	public void watchSongs();
	public void increaseVolume();

}

package testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class ZoomCar extends SeMethods{
	@BeforeTest
	public void setData() {
		testCaseName="ZoomCar";
		testDesc="Test case to execute Zoom car";
		category="Smoke";
		author="Valentina";
	}
	
	@Test
	public void zoomCar(){
		//int i=0;
		
		List <String> value=new ArrayList<>();
		startApp("chrome","https://www.zoomcar.com/chennai/");
		WebElement link = locateElement("link","Start your wonderful journey");
		link.click();
		WebElement popDrop = locateElement("xpath","(//div[@class='items'])[3]");
		popDrop.click();
		WebElement button = locateElement("xpath","//button[text()='Next']");
		button.click();
		int tomorrowDate=getDate();
		WebElement date = locateElement("xpath","//div[text()="+tomorrowDate+"]");
		date.click();
		WebElement button1 = locateElement("xpath","//button[text()='Next']");
		button1.click();
		WebElement button2 = locateElement("xpath","//button[text()='Done']");
		button2.click();
		WebElement table = locateElement("class","car-list-layout");
		List<WebElement> count=table.findElements(By.className("car-listing"));
		System.out.println("The number of rows displayed "+ count.size());
		for (WebElement wele : count) {
			WebElement findElement = wele.findElement(By.className("price"));
			String text = findElement.getText();
			value.add(text.substring(2));
		}
		List<Integer> intList=value.stream().map(s->Integer.parseInt(s)).collect(Collectors.toList());
		int maximum=0, index=0;
		for (int j : intList) {
			if (maximum<j)
				maximum=j;	
		}
		index=intList.indexOf(maximum);
		System.out.println(maximum);
		System.out.println(index);
		String max = Collections.max(value);
		System.out.println(max);
		//340
		WebElement ename = locateElement("xpath","(//div[contains(text(),'"+max+"')]/preceding::div/h3)["+(index)+"]");
		System.out.println(ename.getText());
		WebElement le = locateElement("xpath","(//div[contains(text(),'"+max+"')]/following::button)[1]");
		le.click();
		
	}

	public int getDate() {
		// Get the current date
		Date date = new Date();
// Get only the date (and not month, year, time etc)
		DateFormat sdf = new SimpleDateFormat("dd"); 
// Get today's date
		String today = sdf.format(date);
// Convert to integer and add 1 to it
		int tomorrow = Integer.parseInt(today)+1;
// Print tomorrow's date
		System.out.println(tomorrow);
		return(tomorrow);
	}
	

}

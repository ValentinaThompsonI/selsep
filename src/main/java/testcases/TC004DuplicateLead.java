package testcases;
//import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC004DuplicateLead extends ProjectMethods {
	@BeforeTest(groups="any")
	public void setData() {
		testCaseName="DuplicateLeads";
		testDesc="Test case to execute Duplicate Leads";
		category="Smoke";
		author="Valentina";
	}
	@Test(groups="Regression",dependsOnGroups="Sanity")
	public void duplicateLead()throws InterruptedException {
		//login();
		WebElement findLead=locateElement("link","Find Leads");
		click(findLead);
		WebElement name=locateElement("xpath","(//input[@class=' x-form-text x-form-field'])[25]");
		type(name,"Valentina");
		//Find Leads
		WebElement findLeadButton=locateElement("xpath","//button[text()='Find Leads'] ");
		click(findLeadButton);
		Thread.sleep(5000);
		//(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first'])[1]
		WebElement leadOpen=locateElement("xpath","(//a[@class='linktext'])[4]");
				// (//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first'])[1] ");
		click(leadOpen);
		Thread.sleep(5000);
		WebElement duplicate=locateElement("link","Duplicate Lead");
	    click(duplicate);
	    WebElement phone=locateElement("id","createLeadForm_primaryPhoneNumber");
		type(phone, "9600177958");
		WebElement person=locateElement("id","createLeadForm_primaryPhoneAskForName");
		type(person, "AkshayaThamizh");
		WebElement create1=locateElement("xpath","//input[@class='smallSubmit']");
		click(create1);
		//closeBrowser();
	}

}

package testcases;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.testng.annotations.Test;

public class ExportDataUsingExcel {
	//@Test
	public static Object[][] excel() throws IOException {
		XSSFWorkbook  wb= new XSSFWorkbook("./data/createLead.xlsx");
		XSSFSheet sheet =wb.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		System.out.println(rowCount);
		short cellCount = sheet.getRow(0).getLastCellNum();
		System.out.println(cellCount);
		Object[][] data=new Object[rowCount][cellCount];
		for (int j=1;j<=rowCount;j++) {
		XSSFRow row = sheet.getRow(j);
		for(int i=0;i<cellCount;i++) {
		XSSFCell cell=row.getCell(i);
		try {
			String value = cell.getStringCellValue();
			System.out.println(value);
			data[j-1][i]=value;
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("The value is empty");
			
		}
		}
		}
		wb.close();
		return data;
	}

}

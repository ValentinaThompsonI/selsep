package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC005DeleteLead extends ProjectMethods{
	@BeforeTest(groups="any")
	public void setData() {
		testCaseName="DeleteLeads";
		testDesc="Test case to execute Duplicate Lead";
		category="Smoke";
		author="Valentina";
	}
	//(groups="Regression",dependsOnGroups="Sanity")
	@Test
	public void deleteLead() throws InterruptedException{
		
		WebElement findLead=locateElement("link","Find Leads");
		click(findLead);
		WebElement name=locateElement("xpath","(//input[@class=' x-form-text x-form-field'])[25]");
		type(name,"Valentina");
		//Find Leads
		WebElement findLeadButton=locateElement("xpath","//button[text()='Find Leads'] ");
		click(findLeadButton);
		Thread.sleep(5000);
		WebElement leadOpen=locateElement("xpath","(//a[@class='linktext'])[4]");
		click(leadOpen);
		Thread.sleep(5000);
		WebElement delete=locateElement("xpath","//a[text()='Delete']");
	    click(delete);
	    
		
	}
}

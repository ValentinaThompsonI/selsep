package testcases;

import org.openqa.selenium.WebElement;
//import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC003EditLeads extends ProjectMethods {
	@BeforeTest(groups="any")
	public void setData() {
		testCaseName="EditLeads";
		testDesc="Test case to execute Edit Leads";
		category="Smoke";
		author="Valentina";
	}
	
	@Test(groups="Sanity",dependsOnGroups="Smoke")
	public void EditLeads() throws InterruptedException {
		
		//login();
		WebElement findLead=locateElement("link","Find Leads");
		click(findLead);
		WebElement name=locateElement("xpath","(//input[@class=' x-form-text x-form-field'])[25]");
		type(name,"Valentina");
		//Find Leads
		WebElement findLeadButton=locateElement("xpath","//button[text()='Find Leads'] ");
		click(findLeadButton);
		Thread.sleep(5000);
		//(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first'])[1]
		WebElement leadOpen=locateElement("xpath","(//a[@class='linktext'])[4]");
				// (//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first'])[1] ");
		click(leadOpen);
		Thread.sleep(5000);
		WebElement edit=locateElement("xpath","//a[text()='Edit']");
	    click(edit);
	   /* WebElement eleUserName1 = locateElement("xpath", "(//input[@name='USERNAME'])[1]");
		type(eleUserName1, "DemoSalesManager");
		WebElement elePassword1 = locateElement("xpath","//input[@name='PASSWORD']");
		type(elePassword1, "crmsfa");
		WebElement login1 = locateElement("xpath","(//input[@class='loginButton'])[1]");
	       login1.click();  */
	       WebElement localname1=locateElement("id","updateLeadForm_lastNameLocal");
	   	type(localname1, "Thamizh");  
	   	WebElement update = locateElement("xpath","//input[@value='Update']");
	    click(update); 
	  //  closeBrowser();
	    
	 /*   @AfterTest
		public void close() {
	    	closeBrowser();
		    
		}*/
	    
	}

}

package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

//import wdMethods.SeMethods;

public class TC002LeafTapsTestLead extends ProjectMethods{
	@BeforeTest(groups="any")
	public void setData() {
		testCaseName="CreateLeads";
		testDesc="Test case to execute Create Leads";
		category="Smoke";
		author="Valentina";
	}

	@Test(groups="Smoke",dataProvider="qa")
	public void CreateLead(String cName,String fname,String lname) {
		/*startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);*/
		//login();
			
		WebElement eleCLeads= locateElement("link","Create Lead");
		click(eleCLeads);
		WebElement companyName = locateElement("createLeadForm_companyName");
		type(companyName, cName);
		WebElement firstName = locateElement("createLeadForm_firstName");
		type(firstName, fname);
		WebElement lastName = locateElement("createLeadForm_lastName");
		type(lastName, lname);
		WebElement submit = locateElement("class","smallSubmit");
		click(submit);
		

				
	}
	
	@DataProvider(name="qa")
	public Object[][] fetchData() throws IOException{
		Object[][] data = ExportDataUsingExcel.excel();
		return data;
	}
	/*public String[][] fetchData()
	{
		String[][] data=new String[2][3];
		data[0][0]="Paypal";
		data[0][1]="Anoop";
		data[0][2]="K.C.";
		data[1][0]="Syntel";
		data[1][1]="Avanthika";
		data[1][2]="Anoop";
		
		return data;
	}*/
	
	
}

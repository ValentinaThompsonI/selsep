package testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC006MergeLeads extends TC001Login{
	@BeforeTest(groups="any")
	public void setData() {
		testCaseName="Merge Leads";
		testDesc="Test case to execute Merge Leads";
		category="Smoke";
		author="Valentina";
	}
	@Test
	public void mergeLeads() throws InterruptedException {
		WebElement findLead=locateElement("link","Merge Leads");
		click(findLead);
		////table[@id='widget_ComboBox_partyIdFrom']/following::img
		WebElement clickLead1=locateElement("xpath","(//table[@id='widget_ComboBox_partyIdFrom']/following::img)[1]");
		click(clickLead1);
		Set<String> allWindows = driver.getWindowHandles();
        List<String> lst=new ArrayList<>();
        lst.addAll(allWindows);
        driver.switchTo().window(lst.get(1));
        System.out.println(driver.getTitle());
        driver.manage().window().maximize();
        WebElement name=locateElement("xpath","(//input[@class=' x-form-text x-form-field'])[2]");
		type(name,"Valentina");
		//Find Leads
		WebElement findLeadButton=locateElement("xpath","//button[text()='Find Leads'] ");
		click(findLeadButton);
		Thread.sleep(5000);
		//(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first'])[1]
		WebElement leadOpen=locateElement("xpath","(//a[@class='linktext'])[4]");
				// (//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first'])[1] ");
		click(leadOpen);
		Thread.sleep(5000);
	}
}

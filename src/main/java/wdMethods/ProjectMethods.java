package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class ProjectMethods extends SeMethods {
	@Parameters ({"url","uname","password"})
	@BeforeMethod(groups="any")
	public void login(String url,String uname,String password) {
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, uname);
		WebElement elePassword = locateElement("password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM = locateElement("link","CRM/SFA");
		click(eleCRM);
		WebElement eleLeads= locateElement("link","Leads");
		click(eleLeads);
		
	}
@AfterMethod(groups="any")
public void closeApp() {
	closeBrowser();
}

}

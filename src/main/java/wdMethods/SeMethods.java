package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

//import com.aventstack.extentreports.Status;

import utils.Report;

public class SeMethods extends Report implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;

	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {			
				System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./Drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" Launched Successfully");
			reportStep("pass","The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			System.out.println("The Browser "+browser+" not Launched ");
			reportStep("FAIL","The Browser "+browser+" did not Launch");
		} finally {
			takeSnap();			
		}
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "link" : return driver.findElementByLinkText(locValue);
			}reportStep("pass","The element "+locator+" found Successfully");
		} catch (NoSuchElementException e) {
			System.out.println("The Element Is Not Located ");
			reportStep("FAIL","The element "+locator+" not found");
		}finally{
			takeSnap();			
		}
		return null;
	}

	
	public WebElement locateElement(String locValue) {
		try{
			 WebElement findElementById = driver.findElementById(locValue);
			reportStep("pass","The element "+locValue+" found Successfully");
			return findElementById;
		}catch(NoSuchElementException e) {
			System.out.println("The Element Is Not Located ");
			reportStep("FAIL","The element "+locValue+" not found");
		}
		finally{
			takeSnap();			
		}
		return null;
	}

	@Override
	public void type(WebElement ele, String data) {
		try{
			ele.sendKeys(data);
		System.out.println("The Data "+data+" is Entered Successfully");
		reportStep("PASS","The Data "+data+" is Entered Successfully");
		}catch(WebDriverException e)
		{
			reportStep("FAIL","The Data "+data+" is not entered");
		} finally {
			takeSnap();			
		}
		}
			
		
	@Override
	public void click(WebElement ele) {
		try{
			ele.click();
		System.out.println("The Element "+ele+" Clicked Successfully");
		reportStep("PASS","The Element "+ele+" Clicked Successfully");
		}catch(WebDriverException e)
		{
			System.out.println("The Element "+ele+" is not clicked");
			reportStep("FAIL","The Element "+ele+" is not clicked");
		} finally {
			takeSnap();			
		}
		}
	

	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try{Select dd = new Select(ele);
		dd.selectByVisibleText(value);
		System.out.println("The DropDown Is Selected with "+value);
		reportStep("PASS","The DropDown Is Selected with  "+value);
		}catch(WebDriverException e)
		{
			reportStep("FAIL","The value "+value+" is not selected in the dropdown");
		} finally {
			takeSnap();			
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		Select dd = new Select(ele);
		dd.selectByIndex(index);
		System.out.println("The DropDown Is Selected with index"+index);

	}
	
	public void selectDropDownUsingValue(WebElement ele, String value) {
		Select dd = new Select(ele);
		dd.selectByValue(value);
		System.out.println("The DropDown Is Selected with "+ value);

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		if (driver.getTitle()==expectedTitle)
			return true;
			else
				return false;
		
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		if (ele.getText()==expectedText)
			System.out.println( "The text match") ;
			else
				System.out.println( "The text match");
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		if (ele.getText().contains(expectedText))
			System.out.println( "The partial text is available") ;
			else
				System.out.println( "The partial text is not available");
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		if (ele.isSelected())
			System.out.println( "The WebElement" +ele+"is selected") ;
			else
				System.out.println( "The WebElement" +ele+"is not selected");
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		if (ele.isDisplayed())
			System.out.println( "The WebElement" +ele+"is displayed") ;
			else
				System.out.println( "The WebElement" +ele+"is not displayed");
	}

	@Override
	public void switchToWindow(int index) {
		Set<String> allWindows = driver.getWindowHandles();
		List<String> listOfWindow = new ArrayList<String>();
		listOfWindow.addAll(allWindows);
		driver.switchTo().window(listOfWindow.get(index));
		System.out.println("The Window is Switched ");
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
driver.switchTo().frame(ele);
	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		//takeSnap();
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().accept();
	}

	@Override
	public String getAlertText() {
		String alertText=driver.switchTo().alert().getText();
		return alertText;
	}
	public void alertTypeText(String keysToSend) {
		driver.switchTo().alert().sendKeys(keysToSend);;
		
	}
	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
		// TODO Auto-generated method stub

	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
driver.quit();
	}

}

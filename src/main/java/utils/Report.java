	package utils;

	import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
	import com.aventstack.extentreports.ExtentTest;
	import com.aventstack.extentreports.Status;
	import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

	public abstract class Report {
		public static ExtentTest logger ;
		public String testCaseName, testDesc, category , author;
		public  static ExtentHtmlReporter html ;
		public static ExtentReports extent;
	@BeforeSuite(groups="any")
		public void startResult() {
			html = new ExtentHtmlReporter("./reports/result.html");
			html.setAppendExisting(true);
			extent = new ExtentReports();
			extent.attachReporter(html);
		}
		
		
		public void reportStep(String pass, String desc) {
			if (pass.equalsIgnoreCase("pass")) {
				logger.log(Status.PASS, desc);			
			} else if (pass.equalsIgnoreCase("fail")) {
				logger.log(Status.FAIL, desc);			
			}
		}
		@AfterSuite(groups="any")
		public void endResult() {
			extent.flush();
		}
		
		
		@BeforeMethod(groups="any")
		public void beforeMethod() {
			logger = extent.createTest(testCaseName, testDesc);
			logger.assignAuthor(author);
			logger.assignCategory(category);		
		}
		
		
		
		
		
		
		
		
		
	}



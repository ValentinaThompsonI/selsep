package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandlesAndSnapshot {

	public static void main(String[] args) throws IOException {
		//Access the web drivers
				System.setProperty("webdriver.chrome.driver","C:\\GradleProject\\Workspace\\Selenium\\Drivers\\chromedriver.exe");
				//Create new ChromeDriver Object
				ChromeDriver driver=new ChromeDriver();
				//Open URL
				driver.get("https://www.irctc.co.in/nget/train-search");
		        driver.manage().window().maximize();
		        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		        driver.findElementByLinkText("AGENT LOGIN").click();
		        driver.findElementByLinkText("Contact Us").click();
		        Set<String> allWindows = driver.getWindowHandles();
		        List<String> lst=new ArrayList<>();
		        lst.addAll(allWindows);
		        driver.switchTo().window(lst.get(1));
		        System.out.println(driver.getTitle());
		        driver.manage().window().maximize();
		        File snap = driver.getScreenshotAs(OutputType.FILE);
		        File obj=new File("./snap/img1.jpeg");
		        FileUtils.copyFile(snap, obj);
		        
		        

	}

}

package week4.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class FramesAndAlert {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver","C:\\GradleProject\\Workspace\\Selenium\\Drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		Thread.sleep(2000);
		driver.switchTo().alert().sendKeys("Valentina");
		driver.switchTo().alert().accept();
		String text = driver.findElementById("demo").getText();
		System.out.println(text);
		if(text.contains("Valentina"))
			System.out.println("Valentina is available in the text");
		driver.switchTo().parentFrame();
		
		//driver.
		// TODO Auto-generated method stub

	}

}

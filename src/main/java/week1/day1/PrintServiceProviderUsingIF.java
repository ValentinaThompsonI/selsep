package week1.day1;
import java.util.Scanner;

public class PrintServiceProviderUsingIF {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	System.out.println("Enter the first 3 digits your mobile number:");	
	Scanner scan= new Scanner(System.in);
    int val=scan.nextInt();
    if(val==944)
    	System.out.println("The number "+val+" is BSNL");
    else if(val==900)
	System.out.println("The number "+val+" is Airtel");
    else if(val==897)
    	System.out.println("The number "+val+" is Idea");
    else if(val==630)
    	System.out.println("The number "+val+" is Jio");
    else
    	System.out.println("Invalid input");
    scan.close();
	

	}

}

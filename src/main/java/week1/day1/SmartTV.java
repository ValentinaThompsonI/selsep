package week1.day1;

public class SmartTV {
	public String brand= "Sony";
	protected int modelNo=562845;
	
	public void watchTV() {
		System.out.println("Watching TV");
	}
	public void connectChannel(String channelname) {
		System.out.println("Loading "+ channelname);
	}
}

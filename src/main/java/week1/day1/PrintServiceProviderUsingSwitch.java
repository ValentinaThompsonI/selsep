package week1.day1;

import java.util.Scanner;

public class PrintServiceProviderUsingSwitch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter the first 3 digits your mobile number:");	
		Scanner scan= new Scanner(System.in);
	    int val=scan.nextInt();
	    
	    switch (val) {
	    
	    case 944:
	    	System.out.println("The number "+val+" is BSNL");
	    	break;
	    case 900:
	    	System.out.println("The number "+val+" is Airtel");
	    	break;
	    case 897:
	    	System.out.println("The number "+val+" is Idea");
	    	break;
	    case 630:
	    	System.out.println("The number "+val+" is Jio");
	    	break;
	    default:
	    	System.out.println("Invalid input");
	    	break;
	    }
	    scan.close();

	}

}
